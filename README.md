# 1. PL-Botto

## Contents
- [1.1 Description](#pl-botto)
    - [1.1 Database](escription)
    - [1.2 Architecture](#architecture)
    - [1.3 Bot Modules](#bot-modules)
        - [1.3.1 PL-Commands](#pl-commands)
        - [1.3.2 PL-Common](#pl-common)
        - [1.3.3 PL-Contest](#pl-contest)
        - [1.3.4 PL-Reactions](#pl-reactions)
        - [1.3.5 PL-Ticketing](#pl-ticketing)
    - [1.4 Installation](#installation)
    - [1.5 Database](#database)
    - [1.6 Database](#license)

## Description
This open-source project is a custom discord bot for the [Photography Lounge](https://discord.gg/photography) discord server. It provides helpful services to server staff (e.g., contest atomization) and the community alike (e.g., ticketing service, image feed back).

## Architecture
The discord bot instances uses the [discord.js](https://discord.js.org/) library for the discord functionality and the [pino](https://www.npmjs.com/package/pino), [pino-loki](https://www.npmjs.com/package/pino-loki) and [mysql](https://www.npmjs.com/package/mysql) libraries for logging and database queries. The tech stack is automatically deployed with GitLab's CI/CD with multiple Alpine Docker containers. The bot consists of the following five modules: PL-Commands, PL-Common, PL-Contest, PL-Reactions, and PL-Ticketing. The Docker architecture is shown in the figure below. Beside the bot instances, there are MySQL, Grafana (Grafana, Promtail, Loki) tech stack containers deployed. The MySQL container is a required container used for data storage. The Grafana stack containers are an optional web application to visualize and analyze the logs created by the Discord bot. As shown in the [database section](## 1.2. Database), only the iss of messages, channels, guilds, users, and emojis provided from discord gets stored and therefore no message content or other private  users data.

![architecture](/figures/PL-Bot Architecture.png)

## Bot Modules

### PL-Commands

PL-Commands sets up the various slash commands for the bot.

### PL-Common

PL-Common provides commonly used functionality like giving roles to members via buttons and drop-down menus, creating a thread for members muted by Zeppelin, and counting the users with the everybody role.

Command reference (moderator & admin only)

    !mute <UserId>
    !mute <UserPing>


### PL-Contest

Automizes the contest loop, contest announcements, and leaderboard calculation.


Command reference (admin & contest helper only)

    Contest System:
    creates, shows the embed of, updates, or removes a contest system 

    /contest_sys create name="<name>" announcement_channel=<channel_id> management_channel=<channel_id> submission_channel=<channel_id> contest_duration=<contest_duration> submission_duration=<submission_duration> submission_duration=<voting_duration> emoji=<emoji id> removal_req_channel=<removal_req_channel> removal_res_channel=<removal_res_channel>  removal_count=<removal_count>

    /contest_sys update id=<system id> optional=[name="<name>", announcement_channel=<channel_id>, management_channel=<channel_id>, submission_channel=<channel_id>, contest_duration=<contest_duration>, submission_duration=<submission_duration>, submission_duration=<voting_duration>, emoji=<emoji id> removal_req_channel=<removal_req_channel> removal_res_channel=<removal_res_channel> removal_count=<removal_count>]

    /contest_sys remove id=<system id>

    /contest_sys show id=<system id>

    Contest:
    adds a contest to, updates a contest in, or shows, removes a contest from the contest list of a contest system

    /contest create contest_system=<system id> theme="<theme>" message_link="<message_link>" optional=[start_timestamp=<start_timestamp>, submission_duration=<submission_duration>, submission_duration=<submission_duration>]

    /contest update contest_id=<contest id> optional=[theme="<theme>", message_link="<message_link>", start_timestamp=<start_timestamp>, submission_duration=<submission_duration>, submission_duration=<submission_duration>]

    /contest show contest_id=<contest id> optional=[show_all=<[true, false]>]

    /contest remove contest_id=<contest id>

    /contest remove contest_id=<contest id>
    /contest removal_request link=<link> description=<description>

    /parrot <user> <duration in hours> <duration in minutes> <duration in seconds>
    /unparrot <user> 

Embeds:

    Contest System Mainpage
    ┌───────────────────┐
    │ Contest System    │
    │ Page: 1/3         │
    │ Id     Name       │
    │ 10     Name_10    │
    │  9     Name_9     │
    │  8     Name_8     │
    │  7     Name_7     │
    │  6     Name_6     │
    │  5     Name_5     │
    │  4     Name_4     │
    │  3     Name_3     │
    │  2     Name_2     │
    │  1     Name_1     │
    │  [sys|1|1]        │
    └───────────────────┘
                ┌ Shows Contest System Subpage with id 8
    ┌───┐┌───┐┌───┐┌───┐┌───┐
    │ 10││ 9 ││ 8 ││ 7 ││ 6 │
    └───┘└───┘└───┘└───┘└───┘
    ┌───┐┌───┐┌───┐┌───┐┌───┐
    │ 5 ││ 4 ││ 3 ││ 2 ││ 1 │
    └───┘└───┘└───┘└───┘└───┘
    ┌───┐┌───┐┌───┐┌───┐┌───┐
    │ « ││ < ││ _ ││ » ││ > │
    └───┘└───┘└───┘└───┘└───┘
    └ Page Change  

    Contest System Subpage
    ┌────────────────────────┐
    │ Contest System Subpage │
    │ Id:                 10 │
    │ Name:          Theme10 │
    │ Emoji:           <#id> │
    │ Channels:              │
    │ Management:      <#id> │
    │ Announcement:    <#id> │
    │ Submission:      <#id> │
    │ Durations:             │
    │ Contest:            14 │
    │ Submissions:        2  │
    │ Voting:             2  │
    │ [sys|1|1]              │
    └────────────────────────┘
    ┌───────┐
    │ Back  │
    └───────┘
    │  
    └ Back to Contest System Mainpage



    Contest List Main Page
    ┌─────────────────────────────────────────┐
    │ Contest List Main Page                  │
    │ Id | Theme     Startdate           Link │
    │ 10 | Theme10   October 19, 2022    Link │
    │  9 | Theme9    October 18, 2022    Link │
    │  8 | Theme8    October 17, 2022    Link │
    │  7 | Theme7    October 16, 2022    Link │
    │  6 | Theme6    October 15, 2022    Link │
    │  5 | Theme5    October 14, 2022    Link │
    │  4 | Theme4    October 13, 2022    Link │
    │  3 | Theme3    October 12, 2022    Link │
    │  2 | Theme2    October 11, 2022    Link │
    │  1 | Theme1    October 10, 2022    Link │
    └─────────────────────────────────────────┘
                ┌ Shows Contest System Subpage with id 8
    ┌───┐┌───┐┌───┐┌───┐┌───┐
    │ 10││ 9 ││ 8 ││ 7 ││ 6 │
    └───┘└───┘└───┘└───┘└───┘
    ┌───┐┌───┐┌───┐┌───┐┌───┐
    │ 5 ││ 4 ││ 3 ││ 2 ││ 1 │
    └───┘└───┘└───┘└───┘└───┘
    ┌───┐┌───┐┌───┐┌───┐┌───┐
    │ « ││ < ││ _ ││ » ││ > │
    └───┘└───┘└───┘└───┘└───┘
    └ Page Change  

    Contest Subpage 
    ┌─────────────────────────────────────────┐
    │ Contest Subpage                         │
    │ Id:    10 [x]                           │
    │ Theme: Theme10                          │
    │ Start: October 10, 2022 10:00 AM        │
    │ Vote:  October 17, 2022 10:00 AM        │
    | End:   October 19, 2022 10:00 AM        │
    │ Link:  Msg Link                         │
    └─────────────────────────────────────────┘
    ┌───────┐
    │ Back  │
    └───────┘
    │  
    └ Back to Contest List Main Page


	┌──────────────────────┐
	│ Contest Leaderboard  │
	│ Page: 1/3            │
	│ Votes   User    Link │
	│ 100     User1   Link │
	│  99     User2   Link │
	│  98     User3   Link │
	│  97     User4   Link │
	│  96     User5   Link │
	│  95     User6   Link │
	│  94     User7   Link │
	│  93     User8   Link │
	│  92     User9   Link │
	│  91     UserA   Link │
	│[<con_id>|1|3]        │
	└──────────────────────┘
	┌───┐┌───┐┌───┐┌───┐┌───┐
	│ « ││ < ││ _ ││ » ││ > │
	└───┘└───┘└───┘└───┘└───┘
	└ Page Change  

### PL-Reactions
 
Automatically reacts to posts in image channels with by the config provided emojis. One of the emojis can be used to copy the image of the post to a feedback channel to comment on the post. Upon reaching a configured threshold of upvotes, the post gets copied to a channel for preselection by the specialists to feature posts.

Command reference (specialists & admin only)

    /featurepost link=<link> optional=[description="<description>"]

### PL-Ticketing
Provides a ticketing system that creates private chats on button presses for various use cases.

Command reference (moderator & admin only)

    Ticket System:
    creates, updates, or removes of a ticket system 

    /ticket_system create channel=<channel_id> message="<message to greet the member in the thread use {user} to  get the user id>"

    /ticket_system update optional=[channel=<channel_id>, new_channel=<new_channel>,  message="<message to greet the member in the thread use {user} to  get the user id>"]

    /ticket_system remove channel=<channel_id>

    /ticket_system button channel=<channel_id> btn_msg="<btn_msg>" btn_emoji=<btn_emoji_id>

## Installation

    deploy database

    in ./module_folder/discord-bot folder

    create bot_secrets.json file:

        {
            "discord":"<Bot Token>",
            "mysql": {
                "host": "<MySQL Host IP>",
                "user": "<MySQL Username>",
                "pass": "<MySQL Password>",
                "port": "<MySQL Port>",
                "db": "<MySQL Database>"
            },
            "loki": {
                "host": "<Pino Loki Host IP/URL:Port>",
                "user": "<Pino Loki Username>",
                "pass": "<Pino Loki Password>"
            }
        }

    create config file from this repo

        npm install --no-bin-links

    adjust logger.js to this if you are not using the Grafana tech stack

        const pinologin		= require("./bot_secrets.json");
        const pino			= require('pino');

        const logger = pino.transport({
        targets: [{
            level: 'warn',
            target: 'pino/file',
            options: {
                destination: file
            }, {
                level: 'info',
                target: 'pino-pretty'
            }]
        })

        module.exports = logger

    start the program

        node index.js

## Database

              Parrot Role                    Featured Preselection                   Ticket System                            Contest System
                   │                                   │                                   │                                         │
                   V                                   V                                   V                                         V
    ┌────────────────────────────┐         ┌────────────────────────────┐      ┌────────────────────────────┐       ┌─────────────────────────────────────┐
    │ roles              key(id) │         │ mod_featured       key(id) │      │ mod_ticket         key(id) │       │ mod_contest                 key(id) │
    ├────────────────────────────┤         ├────────────────────────────┤      ├────────────────────────────┤       ├─────────────────────────────────────┤
    │ id                     INT │         │ guild   (id)      TINYTEXT │      │ id                     INT │    ┌─ │ id                              INT │
    │ guild_id          TINYTEXT ├──┐      │ channel (id)      TINYTEXT │      │ guild   (id)      TINYTEXT │    │  │ name                       TINYTEXT │
    │ role_id           TINYTEXT ├──┤      │ message (id)      TINYTEXT │      │ channel (id)      TINYTEXT │    │  │ guild             (id)     TINYTEXT │
    │ role_name         TINYTEXT │  │      └────────────────────────────┘      │ message               TEXT │    │  │ contest_emoji     (id)     TINYTEXT │
    └────────────────────────────┘  │                                          └────────────────────────────┘    │  │ contest_duration                INT │
                                    │                                                                            │  │    vote_duration                INT │
    ┌────────────────────────────┐  │                                                                            │  │     sub_duration                INT │
    │ tmp_roles roles    key(id) │  │                                                                            │  │ announcement_chan (id)     TINYTEXT │
    ├────────────────────────────┤  │                                                                            │  │   management_chan (id)     TINYTEXT │
    │ id                     INT │  │                                                                            │  │   submission_chan (id)     TINYTEXT │
    │ guild_id          TINYTEXT │<─┤                                                                            │  │  removal_req_chan (id)     TINYTEXT │
    │ role_id           TINYTEXT │<─┘                                                                            │  │  removal_res_chan (id)     TINYTEXT │
    │ user_id           TINYTEXT │                                                                               │  │       removal_cnt               INT │
    │ tsp                    INT │                                                                               │  └─────────────────────────────────────┘
    └────────────────────────────┘                                                                               │
                                                                                                                 │  ┌─────────────────────────────────────┐
                                                                                                                 │  │ con_state          key(contest_sys) │
                                                                                                                 │  ├─────────────────────────────────────┤
                                                                                                                 ├─>│ contest_sys                     INT │
                                                                                                                 │  │ contest                         INT │<─┐
                                                                                                                 │  │ state                           INT │  │
                                                                                                                 │  └─────────────────────────────────────┘  │
                                                                                                                 │                                           │
                                                                                                                 │  ┌─────────────────────────────────────┐  │
                                                                                                                 │  │ con_contest                 key(id) │  │
                                                                                                                 │  ├─────────────────────────────────────┤  │
                                                                                                                 │  │ id                              INT ├──┤
                                                                                                                 └─>│ contest_sys                     INT │  │
                                                                                                                    │ theme                          TEXT │  │
                                                                                                                    │ link                           TEXT │  │
                                                                                                                    │ completed                       INT │  │
                                                                                                                    │ start_msg     (msg id)     TINYTEXT │  │
                                                                                                                    │  end_msg      (msg id)     TINYTEXT │  │
                                                                                                                    │ start_tsp                  DATETIME │  │
                                                                                                                    │  vote_tsp                  DATETIME │  │
                                                                                                                    │   end_tsp                  DATETIME │  │
                                                                                                                    └─────────────────────────────────────┘  │
                                                                                                                                                             │
                                                                                                                    ┌─────────────────────────────────────┐  │
                                                                                                                    │ con_submission              key(id) │  │
                                                                                                                    ├─────────────────────────────────────┤  │
                                                                                                                    │ id                              INT │  │
                                                                                                                    │ contest                         INT │<─┘
                                                                                                                    │ message           (id)     TINYTEXT │
                                                                                                                    │ channel           (id)     TINYTEXT │
                                                                                                                    │ user              (id)     TINYTEXT │
                                                                                                                    │ votes                           INT │
                                                                                                                    └─────────────────────────────────────┘


### 1.5.1 Mysql Statements:

Create User and MySql Database

    CREATE USER 'pl_botto'@'%' IDENTIFIED WITH mysql_native_password BY 'password';

    SHOW GRANTS FOR 'pl_botto'@'%';

    CREATE DATABASE IF NOT EXISTS pl_botto;

    GRANT CREATE, SELECT, INSERT, UPDATE, DELETE ON pl_botto.* TO 'pl_botto'@'%';

Create Database Tables

    CREATE TABLE IF NOT EXISTS `pl_botto`.`mod_featured` (
        `id`                 INT NOT NULL AUTO_INCREMENT,
        `guild`              TINYTEXT NOT NULL,
        `channel`            TINYTEXT NOT NULL,
        `message`            TINYTEXT NOT NULL,
        PRIMARY KEY (`id`)
    );

    CREATE TABLE IF NOT EXISTS `pl_botto`.`mod_ticket` (
        `id`                INT NOT NULL AUTO_INCREMENT,
        `guild`             TINYTEXT NOT NULL,
        `channel`           TINYTEXT NOT NULL,
        `message`           TEXT NULL,
        PRIMARY KEY (`id`)
    );

    CREATE TABLE IF NOT EXISTS `pl_botto`.`mod_contest` (
        `id`                INT NOT NULL AUTO_INCREMENT,
        `name`              TINYTEXT NOT NULL,
        `guild`             TINYTEXT NOT NULL,
        `contest_emoji`     TINYTEXT NOT NULL,
        `contest_duration`  INT NOT NULL,
        `vote_duration`     INT NOT NULL,
        `sub_duration`      INT NOT NULL,
        `announcement_chan` TINYTEXT NOT NULL,
        `management_chan`   TINYTEXT NOT NULL,
        `submission_chan`   TINYTEXT NOT NULL,
        `removal_req_chan`  TINYTEXT NOT NULL,
        `removal_res_chan`  TINYTEXT NOT NULL,
        `removal_cnt`       INT NOT NULL,
        PRIMARY KEY (`id`)
    );


    CREATE TABLE IF NOT EXISTS `pl_botto`.`con_state` (
        `contest_sys`       INT NOT NULL,
        `contest`           INT NOT NULL,
        `state`             INT NOT NULL,
        PRIMARY KEY (`contest_sys`)
    );


    CREATE TABLE IF NOT EXISTS `pl_botto`.`con_contest` (
        `id`                INT NOT NULL AUTO_INCREMENT,
        `contest_sys`       INT NOT NULL,
        `theme`             TEXT NULL,
        `link`              TEXT NULL,
        `completed`         INT NOT NULL,
        `start_msg`         TINYTEXT NULL,
        `end_msg`           TINYTEXT NULL,
        `start_tsp`         DATETIME NULL,
        `vote_tsp`          DATETIME NULL,
        `end_tsp`           DATETIME NULL,
        PRIMARY KEY (`id`)
    );

    CREATE TABLE IF NOT EXISTS `pl_botto`.`con_submission` (
        `id`                INT NOT NULL AUTO_INCREMENT,
        `contest`           INT NOT NULL,
        `message`           TINYTEXT NOT NULL,
        `channel`           TINYTEXT NOT NULL,
        `user`              TINYTEXT NOT NULL,
        `votes`             INT NULL,
        PRIMARY KEY (`id`)
    );

    CREATE TABLE IF NOT EXISTS `pl_botto`.`roles` (
        `id`                INT NOT NULL AUTO_INCREMENT,
        `guild_id`          TINYTEXT NOT NULL,
        `role_id`           TINYTEXT NOT NULL,
        `role_name`         TINYTEXT NOT NULL,
        PRIMARY KEY (`id`)
    );

    CREATE TABLE IF NOT EXISTS `pl_botto`.`tmp_roles` (
        `id`                INT NOT NULL AUTO_INCREMENT,
        `guild_id`          TINYTEXT NOT NULL,
        `role_id`           TINYTEXT NOT NULL,
        `user_id`           TINYTEXT NOT NULL,
        `tsp`               INT NOT NULL,
        PRIMARY KEY (`id`)
    );

## License

GNU General Public License, Version 3

## ASCII Art Special Chars Library:
    ┌───────────────────────┐ ╔═══════════════════════╗ ┌─────────────────────────────────────────────────────────────────────────────────────────┐
    │ │ ─ ┌ ┐ └ ┘ ┬ ┴ ┼ ├ ┤ │ ║ ║ ═ ╔ ╗ ╚ ╝ ╦ ╩ ╬ ╠ ╣ ║ │ █ ▄ ▀ ■ ░ ▒ ▓  ¦ © ® × Ø ø ƒ Æ æ ¥ ¢ £ $ § @ \ / ª ½ ¼ ¾ ¡ « » ¤ µ ≡ ± ‗ ÷ ° ¨ · ¹ ² ³  │
    └───────────────────────┘ ╚═══════════════════════╝ └─────────────────────────────────────────────────────────────────────────────────────────┘
